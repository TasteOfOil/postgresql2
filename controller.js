const pool = require('./connectDB');

const createLesson = async(req,res)=>{
    try{
        const lesson = {
            lessonName: req.body.lessonName,
            teacherName: req.body.teacherName,
            studentCount: req.body.studentCount
        };
        if(!lesson.lessonName || !lesson.studentCount || !lesson.teacherName){
            throw new Error("Data entered incorrectly");
        }
        await pool.query("insert into lessons (lessonName, teacherName, studentCount) values ($1,$2,$3)",
        [lesson.lessonName, lesson.teacherName,lesson.studentCount]);
        console.log("POST lesson");
        res.status(200).send("Data added successfully");
    }
    catch(err){
        console.log(err);
        res.status(500).send("Error 500");
    }
};

const getLessons = async(req,res)=>{
    try{
        const result = await pool.query("select * from lessons");
        console.log("GET lessons");
        res.status(200).send(result.rows);
    }
    catch(err){
        console.log(err);
        res.status(500).send("Error 500");
    }
};

const getLesson = async(req,res)=>{
    try{
        const id = req.params.id;
        const result = await pool.query("select * from lessons where id = $1", [id]);
        if(result.rows.length == 0){
            throw new Error("Lesson not found");
        }
        console.log("GET lesson");
        res.status(200).send(result.rows);
    }
    catch(err){
        console.log(err);
        res.status(500).send("Error 500");
    }
};

const updateLesson = async(req,res)=>{
    try{
        const id = req.params.id;
        const result = await pool.query("select * from lessons where id = $1", [id]);
        if(result.rows.length == 0){
            throw new Error("Lesson not found");
        }

        const lesson = {
            lessonName: req.body.lessonName,
            teacherName:req.body.teacherName,
            studentCount: req.body.studentCount
        }

        if(!lesson.lessonName&&!lesson.teacherName&&!lesson.studentCount){
            throw new Error("Data entered incorrectly");
        }
        await updateLessonParams(lesson,id);
        console.log("PUT lesson");
        res.status(200).send("Data updated successfully");
    }
    catch(err){
        console.log(err);
        res.status(500).send("Error 500");
    }
};

const updateLessonParams = async(lesson,id)=>{
    for(i in lesson){
        if(lesson[i]!=null){
            pool.query(`update lessons set ${i} = $1 where id = $2`, [lesson[i],id]);
        }
    }
};

const deleteLesson = async(req,res)=>{
    try{
        const id = req.params.id;
        const result = await pool.query("select * from lessons where id = $1", [id]);
        if(result.rows.length == 0){
            throw new Error("Lesson not found");
        }
        await pool.query("delete from lessons where id = $1", [id]);
        console.log("DELETE lesson");
        res.status(200).send("Data deleted successfully");
    }
    catch(err){
        console.log(err);
        res.status(500).send("Error 500");
    }
};

module.exports = {createLesson, getLesson,getLessons,
                    updateLesson,deleteLesson};
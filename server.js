const cors = require('cors');
const express = require('express');
const app = express();
const controller = require('./controller');

const port = process.env.PORT||3000;
app.listen(port,()=>{
    console.log(`listening on port:${port}`);
});

app.use(express.json());
app.use(cors({
    origin:"*"
}));

app.get('/', (req,res)=>{
    console.log("It's start page");
    res.status(200).send("Start page");
});

app.get('/api/lessons', controller.getLessons);
app.get('/api/lessons/:id', controller.getLesson);
app.post('/api/lessons', controller.createLesson);
app.put('/api/lessons/:id', controller.updateLesson);
app.delete('/api/lessons/:id', controller.deleteLesson);